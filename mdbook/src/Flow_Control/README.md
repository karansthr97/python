## Flow Control

### **Introduction:**
This lesson will walk you through the control flow, which is the order your python scripts operate. Python uses the typical flow control statements. 

### **Topics Covered:**

* [**Operators**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/operators.html#operators)
* [**I/O Print**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/io_print.html#io-print)
* [**I/O Files**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/io_files.html#io-files)
* [**If, Elif, Else**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/if_elif_else.html#if-elif-else)
* [**While Loops**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/while_loops.html#while-loops)
* [**For Loops**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/for_loops.html#for-loops)
* [**Break and Continue**](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/break_continue.html#break-and-continue)


#### To access the Control Flow slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/Flow_Control/slides/)

