## Object Oriented Programming

### **Introduction:**
This lesson you will walk through Object-oriented Programming, or OOP for short, which bundles attributes and methods into individual objects. Here you will learn how to create and use objects using Object-Oriented Programming principals.

### **Topics Covered:**

* [**Modules**](https://90cos.gitlab.io/public/training/lessons/python/oop/modules.html#modules)
* [**Packages**](https://90cos.gitlab.io/public/training/lessons/python/oop/packages.html#packages)
* [**User Classes Pt. 1**](https://90cos.gitlab.io/public/training/lessons/python/oop/user_classes.html#user-classes-pt1)
* [**User Classes Pt.2**](https://90cos.gitlab.io/public/training/lessons/python/oop/user_classes_pt2.html#user-classes-pt2)
* [**Composition vs Inheritance**](https://90cos.gitlab.io/public/training/lessons/python/oop/user_classes_pt2.html#composition-vs-inheritance)
* [**Exception Handling**](https://90cos.gitlab.io/public/training/lessons/python/oop/exceptions.html#exceptions)
* [**Object Oriented Programming Principles**](https://90cos.gitlab.io/public/training/lessons/python/oop/oop_principles.html#oop-principles)
* [**Object Oriented Programming Terminology Review**](https://90cos.gitlab.io/public/training/lessons/python/oop/oop_terminology.html#oop-terminology-review)


#### To access the Object Oriented Programming slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/oop/slides/)

