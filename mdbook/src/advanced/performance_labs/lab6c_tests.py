import unittest, xmlrunner
from lab6c_exfile import *

class StandardCases(unittest.TestCase):
    def test_standard_case(self):
        result = ('you Have won?', 3, 'nt')
        self.assertEqual(result,xthWordTxt('.txt', 14)) 

class EdgeCases(unittest.TestCase):
    def test_non_text_file(self):
        self.assertEqual('No files found', xthWordTxt('.dat', 14))
    def test_index_oor(self):
        result = ('you. helpful. work.', 3, 'nt')
        self.assertEqual(result, xthWordTxt('.txt', 64))

if __name__ == '__main__':
  with open('unittest.xml', 'w') as output:
    unittest.main(
    testRunner=xmlrunner.XMLTestRunner(output=output), 
    failfast=False, 
    buffer=False, 
    catchbreak=False
    )
