'''
Complete the function below to meet the following requirements:
- Two arguments will be passed, the file extension (as '.xxx') and an integer
- Obtain a directory listing of all files in the current directory with the specified extension
- From all files found, grab the Xth word as defined by the integer parameter
- Compile the Xth word from each file into a single string separated by spaces.
- Newline characters should be removed from the string.
- Return a tuple of the string, the number of files of the specified type that existed, and the OS
- This should work in both Windows and linux systems
- if there are no files of the specified type then return the message "No files found."
- if any of the files do not have an Xth element then just grab the last word. 
'''
import os, subprocess

def getFiles(ext):
    dirlist = os.listdir()
    files = []
    for each in dirlist:
        if ext in each:
            files.append(each)
    if len(files) < 1:
        return None
    files.sort()
    return files 

def xthWordTxt(ext, num):
    files = getFiles(ext)
    if files == None:
        return f"No files found"
    wordlist = [] 
    delim = " "
    for afile in files:
        data = open(afile, 'r')
        content = data.readlines()
        content = delim.join(content).replace("\n","")
        try:
            wordlist.append(content.split(" ")[num - 1])
        except IndexError:
            wordlist.append(content.split(" ")[-1])
        data.close()
    word = delim.join(wordlist)
    return word, len(files), os.name