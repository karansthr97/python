## Python Features

### **Introduction:**
This is an introduction to the structure and use of Python. Here you will learn how to use Python, basic functionality, and how to use the built-in Python documentation.

### **Topics Covered:**

* [**Python Introduction**](https://90cos.gitlab.io/public/training/lessons/python/python_features/python_intro.html)
* [**Python Usage**](https://90cos.gitlab.io/public/training/lessons/python/python_features/python_intro.html#python-usage)
* [**PyDocs**](https://90cos.gitlab.io/public/training/lessons/python/python_features/pydocs_pep8.html#pydocs)
* [**Styling and PEP8**](https://90cos.gitlab.io/public/training/lessons/python/python_features/pydocs_pep8.html#styling-and-pep8)
* [**Introduction to Objects**](https://90cos.gitlab.io/public/training/lessons/python/python_features/objects.html#objects)
* [**Differences between version 2.x and 3.x**](https://90cos.gitlab.io/public/training/lessons/python/python_features/py2_py3.html#py2-vs-py3-differences)
* [**Python Interpetor**](https://90cos.gitlab.io/public/training/lessons/python/python_features/running_python.html#python-interpreter)
* [**Running Python**](https://90cos.gitlab.io/public/training/lessons/python/python_features/running_python.html#running-python)


#### To access the Python Features slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/python_features/slides)

