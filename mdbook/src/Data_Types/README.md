## Data Types

### **Introduction:**
Python uses several data types to store different variables as that all have different functionality. In this lesson you will go over the use and structure of the different data types.

### **Topics Covered:**

* [**Variables**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/variables.html#variables)
* [**Numbers**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/numbers.html#numbers)
* [**Strings**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/strings.html#strings)
* [**Lists**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/lists.html#lists)
* [**Bytes and Bytearray**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/byte_array.html#bytes-and-bytearray)
* [**Tuples**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/tuples.html#tuples-range--buffer) 
* [**Range**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/tuples.html#range) 
* [**Buffer**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/tuples.html#buffer-memoryview)
* [**Dictionaries**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/mapping.html#dictionaries-and-sets)
* [**Sets**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/mapping.html#set-and-frozenset)
* [**Conversion Functions**](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/mapping.html#additional-functionality)

#### To access the Data Types slides please click [here](https://90cos.gitlab.io/public/training/lessons/python/Data_Types/slides/)
